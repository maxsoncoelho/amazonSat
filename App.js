import { StatusBar } from 'expo-status-bar';
import { WebView } from 'react-native-webview';
import { SafeAreaView, StyleSheet } from 'react-native';

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <WebView
        style={{flex: 1.0}}
        mediaPlaybackRequiresUserAction={true}
        // allowsInlineMediaPlayback={true}
        javaScriptEnabled={true}
        source={{ uri: 'https://amazonsat.com' }}
        // source={{ uri: 'https://www.youtube.com/watch?v=t8R0IogSutE' }}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
});
